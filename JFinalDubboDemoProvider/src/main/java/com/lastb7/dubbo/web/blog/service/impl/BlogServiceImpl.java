package com.lastb7.dubbo.web.blog.service.impl;

import com.jfinal.plugin.activerecord.Page;
import com.lastb7.dubbo.common.model.Blog;
import com.lastb7.dubbo.web.blog.service.BlogService;

/**
 * BlogService provider端的实现类
 *
 * @author: lbq
 * @email: 526509994@qq.com
 * @date: 17/4/11
 */
public class BlogServiceImpl implements BlogService {
    
    private Blog blogDao;
    
    /**
     * 通过Spring配置文件注入Blog的dao
     *
     * @param blogDao
     */
    public void setBlogDao(Blog blogDao) {
        this.blogDao = blogDao;
    }
    
    public Blog save(Blog blog) {
        
        if (blog == null) {
            return null;
        }
        blog.save();
        return blog;
    }
    
    public void deleteById(String id) {
        blogDao.deleteById(id);
    }
    
    public void update(Blog blog) {
        if (blog == null) {
            return;
        }
        blog.update();
    }
    
    public Blog findById(String id) {
        Blog blog = blogDao.findById(id);
        return blog;
        
    }
    
    public Page<Blog> paginate(int pageNumber, int pageSize) {
        return blogDao.paginate(pageNumber, pageSize, "select *", "from blog order by id asc");
    }
    
    
}
