package com.lastb7.dubbo.app;

import com.alibaba.druid.util.JdbcConstants;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.lastb7.dubbo.common.model._MappingKit;
import com.lastb7.dubbo.plugin.spring.SpringPlugin;

/**
 * @author: lbq
 * @email: 526509994@qq.com
 * @date: 17/4/11
 */
public class DemoAppProvider {
    public static void main(String[] args) throws InterruptedException {
        // 读取配置文件
        Prop p = PropKit.use("a_little_config.txt");
        
        // 配置Druid数据库连接池插件
        DruidPlugin dp = new DruidPlugin(
                PropKit.get("jdbcUrl"),
                PropKit.get("user"),
                PropKit.get("password").trim(),
                PropKit.get("driverClassName"));
        
        WallFilter wall = new WallFilter();
        wall.setDbType(JdbcConstants.MYSQL);
        dp.addFilter(wall);
        
        // 配置ActiveRecord插件
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
        // 所有映射在 MappingKit 中自动化搞定
        _MappingKit.mapping(arp);
        
        // 配置Spring插件
        SpringPlugin sp = new SpringPlugin("classpath:applicationContext.xml");
        
        // 手动启动各插件
        dp.start();
        arp.start();
        sp.start();
        
        System.out.println("Demo provider for Dubbo启动完成。");
        
        // 没有这一句，启动到这服务就退出了
        Thread.currentThread().join();
        
    }
}
