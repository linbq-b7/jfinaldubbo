package com.lastb7.dubbo.app;

import com.nmtx.doc.core.api.jfinal.JFinalApiDocConfig;

/**
 * @author: lbq
 * @email: 526509994@qq.com
 * @date: 17/4/12
 */
public class JDocMain {
    public static void main(String[] args) {
        new JFinalApiDocConfig("jdoc.properties").setClearSuffix("Controller").start();
    
    }
}
