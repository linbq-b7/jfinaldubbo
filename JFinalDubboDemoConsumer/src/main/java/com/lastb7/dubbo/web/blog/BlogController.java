package com.lastb7.dubbo.web.blog;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.lastb7.dubbo.common.model.Blog;
import com.lastb7.dubbo.web.blog.service.BlogService;
import com.lastb7.dubbo.plugin.spring.Inject;
import com.lastb7.dubbo.plugin.spring.IocInterceptor;

/**
 * 博客
 */
// 由于用到了SpringPlugin插件，因此，“BlogController.java”中就用到与之配套的“Ioc”注解。
// 所有 sql 与业务逻辑写在 Model 或 Service 中，不要写在 Controller 中，养成好习惯，有利于大型项目的开发与维护
// 在类定义上使用@Before(IocInterceptor.class)是为了告诉SpringPlugin插件，此类需要用到Spring的Bean注入
@Before({BlogInterceptor.class, IocInterceptor.class})
public class BlogController extends Controller {
    
    // @Inject.BY_NAME是告诉SpringPlugin插件，blogService字段需要注入，注入的实例是consumer.xml中声明的同名Bean。
    // 这样，类中的各方法就可以使用blogService实例了
    // 注意：是注入，而不是初始化，并且注入的是一个代理（参见Dubbo文档）。BlogService只是个接口，它的实现部署在服务端
    @Inject.BY_NAME
    private BlogService blogService;
    
    /**
     * 博客首页
     * @title 首页
     */
    public void index() {
        setAttr("blogPage", blogService.paginate(getParaToInt(0, 1), 10));
        render("blog.html");
    }
    
    /**
     * 添加博客页面
     */
    public void add() {
        render("add.html");
    }
    
    /**
     * 保存一条博客记录
     * @title 保存博客
     * @param title|标题|String|必填
     * @param content|内容|String|必填
     */
    @Before(BlogValidator.class)
    public void save() {
        Blog blog = getModel(Blog.class);
        blogService.save(blog);
        redirect("/blog");
    }
    
    /**
     * 修改一条博客记录
     * @title 修改博客
     * @param title|标题|String|必填
     * @param content|内容|String|必填
     */
    @Before(BlogValidator.class)
    public void update() {
        Blog blog = getModel(Blog.class);
        blogService.update(blog);
        redirect("/blog");
    }
    
    /**
     * 查找一条博客记录
     * @title 查找博客
     * @param id|博客id|String|必填
     */
    public void edit() {
        setAttr("blog", blogService.findById(getPara()));
    }
    
    /**
     * 删除一条博客记录
     * @title 删除博客
     * @param id|博客id|String|必填
     */
    public void delete() {
        blogService.deleteById(getPara());
        redirect("/blog");
    }
}


