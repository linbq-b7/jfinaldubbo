package com.lastb7.dubbo.common.model;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.TableMapping;
import com.lastb7.dubbo.generator.Table;

import java.util.HashMap;
import java.util.Map;

	public final class _ApiModelKit {

	public static void init(String tableName, String primaryKey,
	                        Class<? extends Model<?>> modelClass, Map<String, Class<?>> attrTypeMapTypeMap) {
		Table table = new Table(tableName, primaryKey, modelClass);
		table.setColumnTypeMap(attrTypeMapTypeMap);
		TableMapping.me().putTable(table);
	}

	public static void initModel() {
		System.out.println("Starting mapping model...");
		Map<String, Class<?>> columnMap = new HashMap<String, Class<?>>();
		columnMap.put("id", java.lang.Integer.class);
		columnMap.put("title", java.lang.String.class);
		columnMap.put("content", java.lang.String.class);
		init("blog", "id", Blog.class, columnMap);

	}

}

