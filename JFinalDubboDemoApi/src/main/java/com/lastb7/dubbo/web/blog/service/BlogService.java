package com.lastb7.dubbo.web.blog.service;

import com.jfinal.plugin.activerecord.Page;
import com.lastb7.dubbo.common.model.Blog;

/**
 * @author: lbq
 * @email: 526509994@qq.com
 * @date: 17/4/11
 */
public interface BlogService {
    
    /**
     * 增加一条博客
     *
     * @param blog
     * @return
     */
    Blog save(Blog blog);
    
    /**
     * 删除一条博客
     *
     * @param id
     */
    void deleteById(String id);
    
    /**
     * 修改一条博客
     *
     * @param blog
     */
    void update(Blog blog);
    
    /**
     * 根据id查找一条博客
     *
     * @param id
     * @return
     */
    Blog findById(String id);
    
    /**
     * 分页查询博客记录
     *
     * @param pageNumber 页码
     * @param pageSize   每页显示条数
     * @return
     */
    Page<Blog> paginate(int pageNumber, int pageSize);
    
    
}
